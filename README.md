This application can used to store your own recipes. It works on WP 7.1.

FEATURES : 
1) Review recipes
2) Add recipes
3) Edit recipes
4) Delete recipes
5) Send a recipe via your email